package com.deveducation.www;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MathTest {
    Math cut = new Math();

    static Arguments[] getFlightDegreeTestArgs(){
        return new Arguments[]{
            Arguments.arguments(32 , 5.5, 19.92, 0.01),
            Arguments.arguments(1.1 , 99.99, -0.04, 0.01)
        };
    }

    static Arguments[] getFlightRadianTestArgs(){
        return new Arguments[]{
                Arguments.arguments(32 , 2.5, -100.12, 0.01),
                Arguments.arguments(60, 0.6, 342.14, 0.01)
        };
    }

    static Arguments[] getDistanceCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(55.0, 170.0, 0.56, 3.0, 675.56, 0.01),
                Arguments.arguments(16.7, 12.13, 45, 1.9, 99.77, 0.01)
        };
    }

    static Arguments[] figureTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2.0, 2.0, 1),
                Arguments.arguments(0.4, 0.2, 1),
                Arguments.arguments(-0.5, -1.5, 0)
        };
    }

    static Arguments[] getValueTaskTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4.7, 6.00, 0.01),
                Arguments.arguments(5.8, 8.65, 0.01)
        };
    }

    @ParameterizedTest
    @MethodSource("getFlightDegreeTestArgs")
    void getFlightDegreeTest(double startingSpeed, double angle, double expected, double error){
        double actual = cut.getFlightDegree(startingSpeed, angle);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("getFlightRadianTestArgs")
    void getFlightRadianTest(double startingSpeed, double angle, double expected, double error){
        double actual = cut.getFlightRadian(startingSpeed, angle);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("getDistanceCarsTestArgs")
    void getDistanceCarsTest(double speedCar1, double speedCar2, double initialDistance, double time,
                             double expected, double error){
        double actual = cut.getDistanceCars(speedCar1, speedCar2, initialDistance, time);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("figureTestArgs")
    void figureTest(double coordinateX, double coordinateY, int expected){
        double actual = cut.figure(coordinateX, coordinateY);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("getValueTaskTestArgs")
    void getValueTaskTest(double x, double expected, double error){
        double actual = cut.getValueTask(x);
        Assertions.assertEquals(expected, actual, error);
    }
}
