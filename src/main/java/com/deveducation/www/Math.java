package com.deveducation.www;

public class Math {
    double startSpeed;
    double angle;

    public Math() {
        this.startSpeed = startSpeed;
        this.angle = angle;
    }


    public double getFlightDegree(double startSpeed, double angle){
        double result = ((startSpeed * startSpeed) * java.lang.Math.sin(java.lang.Math.toRadians(2 * angle))) / 9.80665;
        return result;
    }

    public double getFlightRadian(double startSpeed, double angle){
        double result = ((startSpeed * startSpeed) * java.lang.Math.sin(2 * angle)) / 9.80665;
        return result;
    }

    public double getDistanceCars(double speedCar1, double speedCar2, double Distance, double time){
        double endDistance = Distance + speedCar1 * time + speedCar2 * time;
        return endDistance;
    }

    public int figure(double coordinateX, double coordinateY){
        int lies;
        if(((coordinateX >= 0) && (coordinateY >= 1.5 * coordinateX - 1) && (coordinateY <= coordinateX)) ||
                ((coordinateX <= 0) && (coordinateY >= -1.5 * coordinateX - 1) && (coordinateY <= -coordinateX))){

            lies = 1;
            // inside


        }
        else{
            lies = 0;
            //outside
        }
        return lies;
    }

    public double getValueTask(double x){
        double result;
        double valueFirst;
        double valueSecond;
        double expressionFirst = 6 * java.lang.Math.log1p(java.lang.Math.pow(java.lang.Math.E, x + 1) + 2 * java.lang.Math.pow(java.lang.Math.E, x) * java.lang.Math.cos(x));
        double devideFirst = java.lang.Math.log1p(x - java.lang.Math.pow(java.lang.Math.E, x + 1) * java.lang.Math.sin(x));
        double expressionSecond = java.lang.Math.cos(x);
        double devideSecond = java.lang.Math.pow(java.lang.Math.E, java.lang.Math.sin(x));
        valueFirst = expressionFirst / devideFirst;
        valueSecond = java.lang.Math.abs(expressionSecond / devideSecond);
        result = valueFirst + valueSecond;
        return result;
    }
}
